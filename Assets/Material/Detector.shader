﻿Shader "Unlit/Detector"
{
	Properties
	{
		_TexFar("TextureFar", 2D) = "white" {}
		_TexMiddle("TextureMiddle", 2D) = "white" {}
		_TexNear("TextureNear", 2D) = "white" {}
		_DetectRangeFar ("DetectRangeFar", Float) = 0
		_DetectRangeMiddle ("DetectRangeMiddle", Float) = 0
		_DetectRangeNear ("DetectRangeNear", Float) = 0
		_LineColor("LineColor", Color) = (0,0,0,0)
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _TexFar;
			float4 _TexFar_ST;
			sampler2D _TexMiddle;
			float4 _TexMiddle_ST;
			sampler2D _TexNear;
			float4 _TexNear_ST;
			float _DetectRangeFar;
			float _DetectRangeMiddle;
			float _DetectRangeNear;
			float4 _LineColor;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				//fixed4 col = tex2D(_TexFar, (i.uv) / (_DetectRangeNear*2) - 0.5);
				//return col;

				float len = length(i.uv-0.5);
				if (len < _DetectRangeNear)
				{
					fixed4 col = tex2D(_TexNear, i.uv);
					col.a = 0.5;
					return col;
				}
				else if (len < _DetectRangeNear + 0.005)
				{
					return _LineColor;
				}
				else if (len < _DetectRangeMiddle)
				{
					fixed4 col = tex2D(_TexMiddle, i.uv);
					col.a = 0.5;
					return col;
				}
				else if (len < _DetectRangeMiddle + 0.005)
				{
					return _LineColor;
				}
				else if (len < _DetectRangeFar - 0.005)
				{
					fixed4 col = tex2D(_TexFar, i.uv);
					return col;
				}
				else if (len < _DetectRangeFar)
				{
					return _LineColor;
				}
				
				return 0;
			}
			ENDCG
		}
	}
}
