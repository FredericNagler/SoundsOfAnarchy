﻿using UnityEngine;

public class Player : Singleton<Player>
{
    public GameConfig Config;

    private float lookAtX;
    private float lookAtY;

    public void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (Mathf.Abs(x) > 0.1f)
        {
            this.transform.position += Vector3.right * Config.PlayerMovementSpeed * x * Time.deltaTime;
            lookAtX = x;
        }
        
        if (Mathf.Abs(y) > 0.1f)
        {
            this.transform.position += Vector3.forward * Config.PlayerMovementSpeed * y * Time.deltaTime;
            lookAtY = y;
        }

        this.transform.position = new Vector3(
            Mathf.Clamp(this.transform.position.x, Config.MinPlayerMoveArea.x, Config.MaxPlayerMoveArea.x),
            transform.position.y,
            Mathf.Clamp(this.transform.position.z, Config.MinPlayerMoveArea.z, Config.MaxPlayerMoveArea.z));

        var fledermaus = FledermausController.GetNearestFledermaus();
        var distance = (fledermaus.transform.position - transform.position).magnitude;
        if (distance < fledermaus.FledermausConfig.DetectFar && !fledermaus.IsStunned())
        {
            GameHud.Instance.ShowFight(fledermaus);
        }
        else
        {
            GameHud.Instance.HideFight();
        }

        var goal = Quaternion.LookRotation(new Vector3(lookAtX,0,lookAtY));
        transform.rotation = Quaternion.RotateTowards(transform.rotation, goal, 100 * Time.deltaTime);
    }

    public void Spawn(NestController nestController)
    {
        transform.position = nestController.transform.position;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        var corner1 = new Vector3(Config.MinPlayerMoveArea.x, 0, Config.MinPlayerMoveArea.z);
        var corner2 = new Vector3(Config.MinPlayerMoveArea.x, 0, Config.MaxPlayerMoveArea.z);
        var corner3 = new Vector3(Config.MaxPlayerMoveArea.x, 0, Config.MaxPlayerMoveArea.z);
        var corner4 = new Vector3(Config.MaxPlayerMoveArea.x, 0, Config.MinPlayerMoveArea.z);

        Gizmos.DrawLine(corner1, corner2);
        Gizmos.DrawLine(corner2, corner3);
        Gizmos.DrawLine(corner3, corner4);
        Gizmos.DrawLine(corner4, corner1);
    }
}
