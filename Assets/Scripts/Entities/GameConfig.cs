﻿using UnityEngine;

public class GameConfig : ScriptableObject
{
    public float PlayerMovementSpeed;

    public Vector3 MinPlayerMoveArea;

    public Vector3 MaxPlayerMoveArea;

    public float StunTime;

    public float GameRoundInSec;
}
