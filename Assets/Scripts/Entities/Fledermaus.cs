﻿using UnityEngine;

public class Fledermaus : ScriptableObject
{
    public float SpeedSearch;

    public float SpeedFollow;

    public float SpeedAttack;

    public float WaitTime;

    public float FrequencyMin;

    public float FrequencyMax;

    public float EvadeRadius;

    public float MaxMoveRange;

    public float DetectNest;

    public float DetectFar;

    public float DetectFollow;

    public float DetectAttack;

    public float DetectKill;

    public float MaxFrequencyDeviation;

    public float FrequencyMatchTime;

    public float InitialWaitTime;
}
