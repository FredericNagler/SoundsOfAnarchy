﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Input;
using Game;

public class Wave : MonoBehaviour {
    
    private float WaveValue = 0;
    private float CurrentWaveValue = 0;
    
    public float MaxWaveValue = 50;
    public float Tolerance = 2f;
    public bool RightFrequenze = false;

    public UIMeshLine PlayerWave;
    public UIMeshLine EnemyWave;

    public float PlayerWaveInput = 4f;
    public float WaveUpSpeed = 10;
    public float WaveTimeDown = 4f;
    public float totalLength = 500;

    public GameConfig Config;

    public Animator FightPanelAnimator;

    private bool isShown;

    private FledermausController currentFledermaus;

    private float matchTime;

    private bool isMatchShown = false;

    // Use this for initialization
	void Awake ()
    {
        InputManager.Initialize();
        InputManager.AddAlias("WaveUP", EnumInput.GAMEPAD_0_BUTTON); // a
        InputManager.AddAlias("WaveUP", EnumInput.SPACE); // a

        UpdateWave(0, PlayerWave);
    }

    public void Show(FledermausController fledermaus)
    {
        this.WaveValue = 0;
        this.CurrentWaveValue = 0;
        UpdateWave(0, PlayerWave);
        Debug.Log(fledermaus.Frequency);
        UpdateWave(fledermaus.Frequency, EnemyWave);
        isShown = true;
        currentFledermaus = fledermaus;
        matchTime = 0;
    }

    public void Hide()
    {
        isShown = false;
    }
	
	// Update is called once per frame
	void Update()
    {
        if (!isShown)
            return;

        if (InputManager.IsJustPressed("WaveUP"))
        {
            WaveValue = WaveValue + PlayerWaveInput;
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ASpacebar);
        }

        WaveValue -= Time.deltaTime*WaveTimeDown;
        WaveValue = Mathf.Clamp(WaveValue, 0, MaxWaveValue);

        this.CurrentWaveValue = Mathf.Lerp(this.CurrentWaveValue, this.WaveValue, Time.deltaTime * WaveUpSpeed);
        UpdateWave(this.CurrentWaveValue, PlayerWave);

        if(Mathf.Abs(this.CurrentWaveValue - currentFledermaus.Frequency) < this.currentFledermaus.FledermausConfig.MaxFrequencyDeviation)
        {
            matchTime += Time.deltaTime;
            if(matchTime > this.currentFledermaus.FledermausConfig.FrequencyMatchTime)
            {
                currentFledermaus.Stun();
                GameHud.Instance.HideFight();
            }

            if(!isMatchShown)
            {
                FightPanelAnimator.SetTrigger("enable");
                isMatchShown = true;
            }
        }
        else
        {
            matchTime = 0;
            
            if (isMatchShown)
            {
                FightPanelAnimator.SetTrigger("disable");
                isMatchShown = false;
            }
        }
    }

    public void UpdateWave(float playerInput,UIMeshLine Target )
    {
        int pointCount = Target.GetPointCount();
        for (int i = 0; i < pointCount; i++)
        {
            UpdatePoint(i, (float)i / pointCount, playerInput, Target);
        }
    }

    void UpdatePoint(int idx, float value, float PlayerInput, UIMeshLine Target)
    {
        LinePoint point = new LinePoint();
        point.point = new Vector2(value * totalLength, Mathf.Sin((value-0.5f)*2* PlayerInput)*100);
        point.isNextCurve = false;
        point.width = 10;
        point.nextCurveDivideCount = 1;
        Target.SetPointInfo(idx, point);
    }
}
