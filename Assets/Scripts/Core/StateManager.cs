﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class StateManager<StateManagerType> : Singleton<StateManagerType> where StateManagerType : MonoBehaviour
{
    public string FirstState;

    public CanvasGroup FadeGroup;

    private bool IsSwitchingState = false;

    public static bool StartFirstState = true;

    public void SwitchToState(string _state, System.Object _payLoad = null)
    {
        if (IsSwitchingState)
            return;

        StartCoroutine(LoadAsync(_state));
    }

    private void UpdateFade(float _value)
    {
        FadeGroup.alpha = _value;
    }

    private IEnumerator LoadAsync(string _state)
    {
        IsSwitchingState = true;
        var async = SceneManager.LoadSceneAsync(_state);
        async.allowSceneActivation = false;
        
        //iTween.ValueTo(gameObject,iTween.Hash("from",0,"to",1,"time",1,"onupdate","UpdateFade"));
        yield return new WaitForSeconds(1);

        while (!async.isDone && async.progress < 0.9f)
        {
            yield return async.isDone;            
        }
        
        async.allowSceneActivation = true;
        Debug.Log(_state +" loaded!");

        //iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "time", 1, "onupdate", "UpdateFade"));
        yield return new WaitForSeconds(1);

        yield return async;
        IsSwitchingState = false;
    }

    // Use this for initialization
    public virtual void Start()
    {
        DontDestroyOnLoad(gameObject);

        if (StartFirstState)
        {
            SwitchToState(FirstState);
        }
    }
}
