﻿using UnityEngine;

public class Singleton<SingletonType> : MonoBehaviour 
    where SingletonType : MonoBehaviour
{
    static SingletonType mInstance;

    public static SingletonType Instance
    {
        get
        {
            if(mInstance == null)
            {
                mInstance = FindObjectOfType<SingletonType>();
            }
            return mInstance;
        }
    }
}