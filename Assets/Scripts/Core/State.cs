﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class State : MonoBehaviour
    {
        public virtual void Leave()
        {
            Application.Quit();
        }

        public virtual void Awake()
        {
            if (GameManager.Instance != null)
                return;

            GameManager.StartFirstState = false;
            SceneManager.LoadScene("main", LoadSceneMode.Additive);
        }

        // Update is called once per frame
        public virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Leave();
            }
        }

        public void Lock()
        {
            GetComponent<CanvasGroup>().interactable = false;
        }

        public void UnLock()
        {
            GetComponent<CanvasGroup>().interactable = true;
        }
    }
}