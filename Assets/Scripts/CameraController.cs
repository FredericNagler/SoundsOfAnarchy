﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform Target;

    public Vector3 Offset;

    public float Speed;

    public float Distance;

    public Vector3 Min;

    public Vector3 Max;

    // Update is called once per frame
    void Update () {
        var target = Target.position + this.Offset - this.transform.forward * Distance;

        transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime *this.Speed);

        this.transform.position = new Vector3(
            Mathf.Clamp(this.transform.position.x, Min.x, Max.x),
            transform.position.y,
            Mathf.Clamp(this.transform.position.z, Min.z, Max.z));
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        var corner1 = new Vector3(Min.x, 0, Min.z);
        var corner2 = new Vector3(Min.x, 0, Max.z);
        var corner3 = new Vector3(Max.x, 0, Max.z);
        var corner4 = new Vector3(Max.x, 0, Min.z);

        Gizmos.DrawLine(corner1, corner2);
        Gizmos.DrawLine(corner2, corner3);
        Gizmos.DrawLine(corner3, corner4);
        Gizmos.DrawLine(corner4, corner1);
    }
}
