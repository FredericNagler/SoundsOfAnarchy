﻿using UnityEngine.UI;
namespace Game
{
    public class IntroState : State
    {
        public Button SkippButton;
        private void Start()
        {
            Invoke("WakeUp", 2.5f);
        }

        public void WakeUp()
        {
            SkippButton.gameObject.SetActive(true);
        }

        public void GoToGameState()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("game");
        }
    }
}