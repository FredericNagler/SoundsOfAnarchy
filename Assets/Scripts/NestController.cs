﻿using System.Collections.Generic;
using UnityEngine;

public class NestController : MonoBehaviour
{
    public Nest Config;

    public static List<NestController> Nester;

    public List<GameObject> Models;

    private int SpawnsLeft;

	// Use this for initialization
	void Start () {
        SpawnsLeft = Config.Spawns;
        EnableModel(SpawnsLeft);
        Nester.Add(this);
    }

    private void EnableModel(int idx)
    {
        foreach(var model in Models)
        {
            model.SetActive(false);
        }

        if(idx > 0)
        {
            Models[idx >= Models.Count ? Models.Count - 1 : idx - 1].SetActive(true);
        }
    }

    public void Spawn()
    {
        SpawnsLeft--;
        SpawnsLeft = Mathf.Max(0, SpawnsLeft);

        EnableModel(SpawnsLeft);
    }

    public bool IsEmpty()
    {
        return SpawnsLeft <= 0;
    }

    public static NestController GetNearestNest(Vector3 position)
    {
        var nearestDistance = float.MaxValue;
        NestController nearestNest = null;
        foreach(var nest in Nester)
        {
            if (nest.IsEmpty())
                continue;

            var distance = (position - nest.transform.position).magnitude;
            if(distance < nearestDistance)
            {
                nearestNest = nest;
                nearestDistance = distance;
            }
        }
        return nearestNest;
    }

    public void DestroyNest()
    {
        SpawnsLeft = 0;
        EnableModel(SpawnsLeft);
    }

    public static bool AllNesterDestroyed()
    {
        foreach (var nest in Nester)
        {
            if (!nest.IsEmpty())
                return false;
        }
        return true;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
