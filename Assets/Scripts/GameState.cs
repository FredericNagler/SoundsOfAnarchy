﻿
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class GameState : State
    {
      

        public override void Awake()
        {
            base.Awake();

            FledermausController.Fledermaeuse = new List<FledermausController>();
            NestController.Nester = new List<NestController>();
        }
        
        public override void Leave()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("menu");
        }
    }
}