﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Game
{

    public class OptionsState : State {

        public Slider Music;
        public Slider Sound;
        private AudioSource SoundAudioSource;
        private AudioSource MusicAudioSource;

        private void Start()
        {
            SoundAudioSource = GameManager.Instance.Sound;
            MusicAudioSource = GameManager.Instance.Music;

            Music.value = MusicAudioSource.volume;
            Sound.value = SoundAudioSource.volume;
        }

        public void OnMusicValueChanged()
        {
            MusicAudioSource.volume = Music.value;
        }

        public void OnSoundValueChanged()
        {
            SoundAudioSource.volume = Sound.value;
        }

        public override void Leave()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("menu");
        }
    }
}