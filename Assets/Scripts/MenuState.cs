﻿using UnityEngine;

namespace Game
{
    public class MenuState : State
    {
        public void Start()
        {

        }

        public void GoToGameState()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("intro");
        }

        public void GoToCreditsState()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("credits");
        }

        public void GoToOptionState()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("option");

        }

        public void GoToCommandsState()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("commands");
        }

     
    }
}