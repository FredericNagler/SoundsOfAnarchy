﻿using UnityEngine.UI;
using UnityEngine;

namespace Game
{   
    public class CreditsState : State
    {
        public override void Leave()
        {
            GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.ButtonSound);
            GameManager.Instance.SwitchToState("menu");
        }
    }
}