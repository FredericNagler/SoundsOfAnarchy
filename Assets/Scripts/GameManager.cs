﻿using UnityEngine;

namespace Game
{
    public class GameManager : StateManager<GameManager>
    {
        public AudioSource Music;

        public AudioSource Sound;
        public AudioClip ButtonSound;
        public AudioClip BatEat;
        public AudioClip MotteDie;
        public AudioClip ASpacebar;
        public AudioClip RightFrequence;
        public AudioClip MotteSpawn;
        public AudioClip HUDopen;
        public AudioClip HUDclose;
        public AudioClip GameLose;
        public AudioClip GameWin;



        public override void Start()
        {
            base.Start();
        }
    }
}