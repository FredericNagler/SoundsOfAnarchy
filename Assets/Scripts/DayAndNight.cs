﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace Game
{
    public class DayAndNight : MonoBehaviour
    {
        public AnimationCurve FadeCurve;

        public ColorCorrectionCurves curve;

        public GameConfig Config;

        private float CurrentTime;

        // Use this for initialization
        void Start()
        {
            CurrentTime = 0;
        }

        // Update is called once per frame
        void Update()
        {
            CurrentTime += Time.deltaTime;
            CurrentTime = Mathf.Min(CurrentTime, Config.GameRoundInSec);

            curve.FadeValue = FadeCurve.Evaluate(1-CurrentTime / Config.GameRoundInSec);

            if(CurrentTime == Config.GameRoundInSec)
            {
                FledermausController.GameOver();
                GameHud.Instance.End(true);
            }
        }
    }
}