﻿using Game;
using System.Collections.Generic;
using UnityEngine;

public enum FledermausState
{
    None,
    Waiting,
    Searching,
    Following,
    Attacking,
    Stun
}

public class FledermausController : MonoBehaviour
{
    public Fledermaus FledermausConfig;

    public GameConfig GameConfig;

    public Transform Detector;

    private Animator animator;

    private FledermausState state;

    private Vector3 target;

    public static List<FledermausController> Fledermaeuse;

    public float Frequency;

    public GameObject StarField;

    private Vector3 lastPosition;

    // Use this for initialization
    void Start ()
    {
        Fledermaeuse.Add(this);

        GenerateNewFrequency();
        this.animator = GetComponent<Animator>();

        this.Detector.localScale = Vector3.one * FledermausConfig.DetectFar * 2;
        var meshRenderer = this.Detector.GetComponent<MeshRenderer>();
        meshRenderer.material.SetFloat("_DetectRangeFar", 0.5f);
        meshRenderer.material.SetFloat("_DetectRangeMiddle", FledermausConfig.DetectFollow / (FledermausConfig.DetectFar * 2));
        meshRenderer.material.SetFloat("_DetectRangeNear", FledermausConfig.DetectAttack / (FledermausConfig.DetectFar * 2));

        this.InitialWait();
    }

    private void GenerateNewFrequency()
    {
        Frequency = Random.Range(FledermausConfig.FrequencyMin, FledermausConfig.FrequencyMax);
    }

    public static FledermausController GetNearestFledermaus()
    {
        var playerPosition = Player.Instance.transform.position;
        var nearestDistance = float.MaxValue;
        FledermausController nearestFledermaus = null;
        foreach (var fledermaus in Fledermaeuse)
        {
            var distance = (playerPosition - fledermaus.transform.position).magnitude;
            if (distance < nearestDistance)
            {
                nearestFledermaus = fledermaus;
                nearestDistance = distance;
            }
        }
        return nearestFledermaus;
    }

    public void Stun()
    {
        GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.RightFrequence);
        state = FledermausState.Stun;
        Invoke("StunFinished", GameConfig.StunTime);
    }

    public bool IsStunned()
    {
        return state == FledermausState.Stun;
    }

    private void StunFinished()
    {
        state = FledermausState.Waiting;
        GenerateNewFrequency();
        WaitFinished();
    }

    private Vector3 GetNewSpawnPoint()
    {
        var newTarget = new Vector3(
            Random.Range(GameConfig.MinPlayerMoveArea.x, GameConfig.MaxPlayerMoveArea.x),
            this.transform.position.y,
            Random.Range(GameConfig.MinPlayerMoveArea.z, GameConfig.MaxPlayerMoveArea.z));

        return newTarget;
    }

    private Vector3 GetNewTarget()
    {
        var newTarget = new Vector3(
            Random.Range(GameConfig.MinPlayerMoveArea.x, GameConfig.MaxPlayerMoveArea.x),
            this.transform.position.y,
            Random.Range(GameConfig.MinPlayerMoveArea.z, GameConfig.MaxPlayerMoveArea.z));

        var vec = (newTarget - transform.position);
        if(vec.magnitude > FledermausConfig.MaxMoveRange)
        {
            newTarget = transform.position + vec.normalized * FledermausConfig.MaxMoveRange;
        }

        return newTarget;
    }

    private void Wait()
    {
        Invoke("WaitFinished", FledermausConfig.WaitTime);
        state = FledermausState.Waiting;
    }

    private void WaitFinished()
    {
        if (state != FledermausState.Waiting)
            return;

        state = FledermausState.Searching;
        target = GetNewTarget();
    }

    private void InitialWait()
    {
        Invoke("WaitFinished", FledermausConfig.InitialWaitTime);
        state = FledermausState.Waiting;
    }

    private FledermausController GetFledermausWhichIsToNear()
    {
        foreach(var fledermaus in Fledermaeuse)
        {
            if (fledermaus == this)
                continue;

            if((fledermaus.transform.position - transform.position).magnitude < FledermausConfig.EvadeRadius + fledermaus.FledermausConfig.EvadeRadius)
            {
                return fledermaus;
            }
        }
        return null;
    }

    private float GetDistanceToPlayer()
    {
        return Vector3.Distance(transform.position, Player.Instance.transform.position);
    }

    private float GetDistanceToTarget()
    {
        return Vector3.Distance(transform.position, target);
    }

    public static void GameOver()
    {
        foreach(var fledermaus in Fledermaeuse)
        {
            fledermaus.enabled = false;
        }
        Player.Instance.enabled = false;
    }

    private void Respawn()
    {
        var nearestNest = NestController.GetNearestNest(Player.Instance.transform.position);
        if(nearestNest == null)
        {
            return;
        }
        nearestNest.Spawn();
        Player.Instance.Spawn(nearestNest);
        Wait();
    }

    private Vector3 GetDeltaPosition(Vector3 to)
    {
        return (to - transform.position).normalized * Time.deltaTime;
    }

    // Update is called once per frame
    void Update ()
    {
        if (NestController.AllNesterDestroyed())
        {
            GameOver();
            GameHud.Instance.End(false);
            return;
        }

        if(lastPosition != transform.position)
        {
            var goal = Quaternion.LookRotation(transform.position - lastPosition);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, goal, 200 * Time.deltaTime);
            lastPosition = transform.position;
        }

        StarField.SetActive(state == FledermausState.Stun);

        if (state == FledermausState.Stun)
            return;

        if(state == FledermausState.Waiting || state == FledermausState.Searching)
        {
            var other = GetFledermausWhichIsToNear();
            if (other != null)
            {
                state = FledermausState.Searching;

                target = transform.position + (transform.position - other.transform.position).normalized * FledermausConfig.EvadeRadius / 2;
                transform.position += (transform.position - other.transform.position).normalized * FledermausConfig.SpeedFollow * Time.deltaTime;
                return;
            }

            if (GetDistanceToPlayer() < FledermausConfig.DetectFollow)
            {
                state = FledermausState.Following;
            }

            if (state == FledermausState.Searching)
            {
                var nearestNest = NestController.GetNearestNest(transform.position);
                if (nearestNest != null)
                {
                    var distance = (nearestNest.transform.position - transform.position).magnitude;
                    if (distance < FledermausConfig.DetectKill)
                    {
                        //Destroy nest
                        nearestNest.DestroyNest();
                        
                        Wait();
                        return;
                    }
                    else if (distance < FledermausConfig.DetectFar)
                    {
                        this.target = nearestNest.transform.position;
                    }
                    
                }

                transform.position += GetDeltaPosition(target) * FledermausConfig.SpeedSearch;


                if(GetDistanceToTarget() < 0.1f)
                {
                    Wait();
                }
            }
        }

        if(state == FledermausState.Following)
        {
            transform.position += GetDeltaPosition(Player.Instance.transform.position) * FledermausConfig.SpeedFollow;

            if(GetDistanceToPlayer() < FledermausConfig.DetectAttack)
            {
                state = FledermausState.Attacking;
            }
        }

        if(state == FledermausState.Attacking)
        {
            transform.position += GetDeltaPosition(Player.Instance.transform.position) * FledermausConfig.SpeedAttack;

            if (GetDistanceToPlayer() < FledermausConfig.DetectKill)
            {
                Respawn();
                GameManager.Instance.Sound.PlayOneShot(GameManager.Instance.MotteDie);
            }
        }
    }
}
