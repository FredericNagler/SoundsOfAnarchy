﻿using Game;
using UnityEngine;

public class GameHud : Singleton<GameHud>
{
    public Animator FightAnimator;

    private bool fightIsShown;

    public GameObject WinPanel;

    public GameObject LostPanel;

    public void ShowFight(FledermausController fledermaus)
    {
        if (fightIsShown)
            return;
        
        FightAnimator.GetComponent<Wave>().Show(fledermaus);
        FightAnimator.SetTrigger("show");
        fightIsShown = true;
    }

    public void HideFight()
    {
        if (!fightIsShown)
            return;

        FightAnimator.GetComponent<Wave>().Hide();
        FightAnimator.SetTrigger("hide");
        fightIsShown = false;
    }

    public void End(bool won)
    {
        if (won)
            WinPanel.SetActive(true);
        else
            LostPanel.SetActive(true);

        Invoke("WaitTimeOver", 5);
    }

    private void WaitTimeOver()
    {
        GameManager.Instance.SwitchToState("menu");
    }
}
